package net.openesb.command.impl;

import net.openesb.command.internal.CommandLineParserFactory;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ListServiceAssembliesCommandTest {
    
    @Test
    @Ignore
    public void execute() {
        String [] args = {"set-jbi-component-logger", "--component", "sun-bpel-engine", "com.sun.jbi.engine.bpel.core.bpel.engine.impl.ScalabilityManager = FINE"};
        CommandLineParserFactory.getInstance().execute(args);
    }
}
