package net.openesb.command.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import net.openesb.command.CommandException;
import net.openesb.model.api.ApplicationVariable;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ApplicationVariableConverterTest {
    
    @Test
    public void testSimpleVariableWithComma() throws CommandException {
	String values = "test=failover:(tcp://host1:1234\\, tcp://host2:1234)";
		
	Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(values));
        } catch (IOException e) {
            properties = createPropertiesParam(values);
        }
	
	Set<ApplicationVariable> variablesArr = new HashSet<ApplicationVariable>();
	
	for (Map.Entry<Object, Object> entry : properties.entrySet()) {
	    System.out.println((String)entry.getKey());
	    System.out.println((String)entry.getValue());
	    
            ApplicationVariable appVariable = ApplicationVariableConverter.convert(
		    (String)entry.getKey(), (String)entry.getValue());

            if (appVariable != null) {
                variablesArr.add(appVariable);
            }
        }
	
	Assert.assertTrue(variablesArr.size() > 0);
	
	ApplicationVariable myVar = variablesArr.iterator().next();
	
	Assert.assertEquals("test", myVar.getName());
	Assert.assertEquals("failover:(tcp://host1:1234, tcp://host2:1234)", myVar.getValue());
    }
    
    @Test
    public void testMultipleVariables() throws CommandException {
	String values = "FirstName=[String]Fred,LastName=Smith,t=[BOOLEAN]true,f=[BOOLEAN]false";
		
	Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(values));
        } catch (IOException e) {
            properties = createPropertiesParam(values);
        }
	
	Set<ApplicationVariable> variablesArr = new HashSet<ApplicationVariable>();
	
	for (Map.Entry<Object, Object> entry : properties.entrySet()) {
	    ApplicationVariable appVariable = ApplicationVariableConverter.convert(
		    (String)entry.getKey(), (String)entry.getValue());

            if (appVariable != null) {
                variablesArr.add(appVariable);
            }
        }
	
	Assert.assertTrue(variablesArr.size() == 4);
    }
    
    /**
     * Formulate and Returns Properties from the given string
     * @return Properties
     */
    protected Properties createPropertiesParam(String propertyStr) throws CommandException
    {
        if (propertyStr == null) return null;
        Properties properties = new Properties();
        final CLITokenizer propertyTok = new CLITokenizer(propertyStr, ",");
        while (propertyTok.hasMoreTokens()) {
            final String nameAndvalue = propertyTok.nextToken();
            final CLITokenizer nameTok = new CLITokenizer(nameAndvalue, "=");
            if (nameTok.countTokens() == 2)
            {
                properties.setProperty(nameTok.nextTokenWithoutEscapeAndQuoteChars(),
                                       nameTok.nextTokenWithoutEscapeAndQuoteChars());
            }
            else
            {
                throw new CommandException("InvalidPropertySyntax");
            }
        }
        return properties;
    }
}
