package net.openesb.command.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.openesb.model.api.ApplicationVariable;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 * @author Pymma consulting Add capability to accept empty value in the
 * application variables
 */
public final class ApplicationVariableConverter {

    private final static Pattern APP_VARIABLE_PATTERN = Pattern.compile(
            "(\\[(boolean|string|number|password)\\])?(.+)", Pattern.CASE_INSENSITIVE);

    private ApplicationVariableConverter() {
    }

    public static ApplicationVariable convert(String key, String inputValue) {

        if (inputValue.startsWith("\"") && inputValue.endsWith("\"")) {
            inputValue = inputValue.substring(1, inputValue.length() - 1);
        }
        Matcher matcher = APP_VARIABLE_PATTERN.matcher(inputValue);
        if (matcher.matches()) {
            ApplicationVariable appVariable = new ApplicationVariable();
            appVariable.setName(key.trim());

            if (matcher.group(1) != null) {
                String type = matcher.group(1);
                String value = matcher.group(0);
                value = value.substring(value.lastIndexOf("]") + 1);
                type = type.substring(1, type.length() - 1).toUpperCase();
                appVariable.setType(type);
                appVariable.setValue(value);
            } else {
                // Support the empty value when the value equals ''
                if ("\'\'".equals(inputValue)) {
                    appVariable.setValue("");
                } else {
                    appVariable.setValue(matcher.group(0));
                }
                appVariable.setType("STRING");
            }
            return appVariable;
        }
        return null;
    }
}
