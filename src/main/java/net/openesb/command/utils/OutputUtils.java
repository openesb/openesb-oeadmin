package net.openesb.command.utils;

/**
 *
 * @author david
 */
public class OutputUtils {
    
    /**
     * Will create a string of the size specified filled with the fillChar.
     *
     * @param fillChar the character to create the string with
     * @param the size of the string
     */
    public static String createFillString(char fillChar, int size) {
        String fillString = "";
        for (int i = 0; i < size; i++) {
            fillString += fillChar;
        }
        return fillString;
    }
    
    /**
     * Will format the description text that is displayed in the show commands.  The
     * formatting will consist of removing all new lines and extra white space, then
     * adding back in line breaks at the first avaliable location that is less then
     * or equal to the given max line length.
     * @param msgText the message string to format
     * @param maxLength the maximum line length size.
     * @param indentAmout the amount to indent row 2 - n
     */
    public static String multiLineFormat (String msgText, int maxLength, int indentAmount)
    {
        // Strip out the leading white spaces in each row, and remove any "\n"
        int endIndex = 0;
        int startIndex = 0;
        String finalString = "";
        String rowString = "";
        String space = "";
        endIndex = msgText.indexOf("\n",startIndex);
        while (endIndex != -1)
        {   
            rowString = msgText.substring(startIndex,endIndex).trim();
            finalString += space + rowString;
            startIndex = endIndex + 1;
            endIndex = msgText.indexOf("\n",startIndex);
            space = " ";
        }
        rowString = msgText.substring(startIndex).trim();
        finalString += space + rowString;

        // Format the string by adding the line breaks in the correct location and adding
        // the indention amount at that beginning of each row.
        endIndex = 0;
        startIndex = 0;
        int spaceIndex = 0;
        int indentSize = 0;
        String newString = "";
        boolean done = false;
        int totalLength = finalString.length();
        while (!(done))
        {
            endIndex = ((startIndex + maxLength) > totalLength) ? totalLength : (startIndex + maxLength);
            rowString = finalString.substring(startIndex,endIndex);
            spaceIndex = startIndex + rowString.lastIndexOf(" ");
            if (endIndex < totalLength)
            {
                spaceIndex = startIndex + rowString.lastIndexOf(" ");
                if (spaceIndex != -1)
                {
                    endIndex = spaceIndex;
                }
            }
            rowString = finalString.substring(startIndex,endIndex) + "\n";
            startIndex = endIndex + 1;
            newString += createFillString(' ',indentSize) + rowString;
            indentSize = indentAmount;
            if (startIndex >= totalLength)
            {
                done = true;
            }
        }
        finalString = newString;
        return finalString;
    }
    
    /**
     * Will display the provided message to the screen.
     * @param msg The message to display
     * @param indentAmount The amount to indent the message by
     */
    public static void displayMessage (String msg, int indentAmount)
    {
        if (msg != null)
        {
            String outputString = createFillString(' ',indentAmount) + msg;
            System.out.println(outputString);
        }
    }
}
