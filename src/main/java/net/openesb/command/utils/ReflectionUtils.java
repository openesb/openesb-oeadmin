package net.openesb.command.utils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ReflectionUtils {

    public static Set<Class<?>> getClassesInPackage(String pkg) {
        Set<Class<?>> commands = new HashSet<Class<?>>();

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(
                null, null, null);

        JavaFileManager.Location location = StandardLocation.CLASS_PATH;
        String packageName = pkg;
        Set<JavaFileObject.Kind> kinds = new HashSet<JavaFileObject.Kind>();
        kinds.add(JavaFileObject.Kind.CLASS);
        boolean recurse = false;

        try {
            Iterable<JavaFileObject> list = fileManager.list(location, packageName,
                    kinds, recurse);

            for (JavaFileObject javaFileObject : list) {
                commands.add(javaFileObject.getClass());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return commands;
    }
}
