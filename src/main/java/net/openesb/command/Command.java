package net.openesb.command;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface Command {
    
    boolean execute();
    
    boolean help();
}
