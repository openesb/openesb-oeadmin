package net.openesb.command.internal;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface CommandLineParser {
    
    void execute(String [] args);
    
    void printUsage();
    
    void printUsage(String command);
}
