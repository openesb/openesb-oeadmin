package net.openesb.command.internal.jcommander;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.ParameterException;
import java.util.Set;
import net.openesb.command.Command;
import net.openesb.command.impl.CreateComponentApplicationConfigurationCommand;
import net.openesb.command.impl.CreateComponentApplicationVariableCommand;
import net.openesb.command.impl.DeleteComponentApplicationConfigurationCommand;
import net.openesb.command.impl.DeleteComponentApplicationVariableCommand;
import net.openesb.command.impl.DeployServiceAssemblyCommand;
import net.openesb.command.impl.InstallComponentCommand;
import net.openesb.command.impl.InstallSharedLibraryCommand;
import net.openesb.command.impl.ListComponentApplicationConfigurationsCommand;
import net.openesb.command.impl.ListComponentApplicationVariablesCommand;
import net.openesb.command.impl.ListComponentsCommand;
import net.openesb.command.impl.ListServiceAssembliesCommand;
import net.openesb.command.impl.ListSharedLibrariesCommand;
import net.openesb.command.impl.SetComponentConfigurationCommand;
import net.openesb.command.impl.SetComponentLoggerCommand;
import net.openesb.command.impl.SetRuntimeConfigurationCommand;
import net.openesb.command.impl.SetRuntimeLoggerCommand;
import net.openesb.command.impl.ShowComponentCommand;
import net.openesb.command.impl.ShowServiceAssemblyCommand;
import net.openesb.command.impl.ShowSharedLibraryCommand;
import net.openesb.command.impl.ShutdownComponentCommand;
import net.openesb.command.impl.ShutdownServiceAssemblyCommand;
import net.openesb.command.impl.StartComponentCommand;
import net.openesb.command.impl.StartServiceAssemblyCommand;
import net.openesb.command.impl.StopComponentCommand;
import net.openesb.command.impl.StopServiceAssemblyCommand;
import net.openesb.command.impl.UndeployServiceAssemblyCommand;
import net.openesb.command.impl.UninstallComponentCommand;
import net.openesb.command.impl.UninstallSharedLibraryCommand;
import net.openesb.command.impl.UpdateComponentApplicationConfigurationCommand;
import net.openesb.command.impl.UpdateComponentApplicationVariableCommand;
import net.openesb.command.impl.UpgradeComponentCommand;
import net.openesb.command.internal.CommandLineParser;
import net.openesb.command.utils.I18NBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class JCommanderCommandLineParser implements CommandLineParser {

    private JCommander parser;

    public JCommanderCommandLineParser() {
        this.init();
    }

    private void init() {
        parser = new JCommander();

        parser.setProgramName("oeadmin");

        parser.addCommand(new CreateComponentApplicationConfigurationCommand());
        parser.addCommand(new CreateComponentApplicationVariableCommand());
        parser.addCommand(new DeleteComponentApplicationConfigurationCommand());
        parser.addCommand(new DeleteComponentApplicationVariableCommand());
        parser.addCommand(new DeployServiceAssemblyCommand());
        parser.addCommand(new InstallComponentCommand());
        parser.addCommand(new InstallSharedLibraryCommand());
        parser.addCommand(new ListComponentApplicationConfigurationsCommand());
        parser.addCommand(new ListComponentApplicationVariablesCommand());
        parser.addCommand(new ListComponentsCommand());
        parser.addCommand(new ListServiceAssembliesCommand());
        parser.addCommand(new ListSharedLibrariesCommand());
        parser.addCommand(new SetComponentConfigurationCommand());
        parser.addCommand(new SetComponentLoggerCommand());
        parser.addCommand(new SetRuntimeConfigurationCommand());
        parser.addCommand(new SetRuntimeLoggerCommand());
        parser.addCommand(new ShowComponentCommand());
        parser.addCommand(new ShowServiceAssemblyCommand());
        parser.addCommand(new ShowSharedLibraryCommand());
        parser.addCommand(new ShutdownComponentCommand());
        parser.addCommand(new ShutdownServiceAssemblyCommand());
        parser.addCommand(new StartComponentCommand());
        parser.addCommand(new StartServiceAssemblyCommand());
        parser.addCommand(new StopComponentCommand());
        parser.addCommand(new StopServiceAssemblyCommand());
        parser.addCommand(new UndeployServiceAssemblyCommand());
        parser.addCommand(new UninstallComponentCommand());
        parser.addCommand(new UninstallSharedLibraryCommand());
        parser.addCommand(new UpdateComponentApplicationConfigurationCommand());
        parser.addCommand(new UpdateComponentApplicationVariableCommand());
        parser.addCommand(new UninstallComponentCommand());
        parser.addCommand(new UpgradeComponentCommand());
    }

    @Override
    public void execute(String[] args) {
        try {
            // Start with command parsing
            parser.parse(args);

            JCommander internalCmd = parser.getCommands().get(
                    parser.getParsedCommand());

            // No-args... so let's print usage to the user.
            if (internalCmd == null) {
                printLightUsage();
                
                return;
            }

            // Lets go with command execution
            Command cmd = (Command) internalCmd.getObjects().iterator().next();

            if (cmd.help()) {
                printUsage(parser.getParsedCommand());
            } else {
                cmd.execute();
            }
        } catch (MissingCommandException ex) {
            System.out.println(
                    I18NBundle.getBundle().getMessage("cli.command.not.found", args));
        } catch (ParameterException pe) {
            System.out.println(pe.getMessage());
        }
    }

    public void printLightUsage() {
        StringBuilder builder = new StringBuilder();
        
        Set<String> availableCommands = parser.getCommands().keySet();
        for (String commandName : availableCommands) {
            builder.append("    ").append(commandName).append("\t\t").append(
                    parser.getCommandDescription(commandName)).append("\n");
        }
        
        System.out.println(builder.toString());
    }

    @Override
    public void printUsage() {
        parser.usage();
    }

    @Override
    public void printUsage(String command) {
        parser.usage(command);
    }
}
