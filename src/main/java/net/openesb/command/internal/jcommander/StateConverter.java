package net.openesb.command.internal.jcommander;

import com.beust.jcommander.IStringConverter;
import net.openesb.model.api.State;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StateConverter implements IStringConverter<State> {

    @Override
    public State convert(String value) {
        return State.from(value);
    }
}
