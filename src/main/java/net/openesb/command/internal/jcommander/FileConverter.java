package net.openesb.command.internal.jcommander;

import com.beust.jcommander.IStringConverter;
import java.io.File;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class FileConverter implements IStringConverter<File> {

    @Override
    public File convert(String value) {
        return new File(value);
    }
}
