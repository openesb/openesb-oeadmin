package net.openesb.command.internal;

import net.openesb.command.internal.jcommander.JCommanderCommandLineParser;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class CommandLineParserFactory {
    
    private static CommandLineParser parser = null;
    
    public synchronized  static CommandLineParser getInstance() {
        if (parser == null) {
            parser = new JCommanderCommandLineParser();
        }
        
        return parser;
    }
}
