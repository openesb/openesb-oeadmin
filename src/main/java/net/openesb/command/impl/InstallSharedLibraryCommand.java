package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.File;
import java.net.URL;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.InstallSharedLibraryRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.INSTALL_SHARED_LIBRARY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.install-jbi-shared-library.descr")
public class InstallSharedLibraryCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.install-jbi-shared-library.param.path",
            arity = 1)
    private List<String> sharedLibraryArchives;

    @Override
    protected void executeRequest() throws Exception {
        String sharedLibraryArchive = sharedLibraryArchives.iterator().next();

        if (sharedLibraryArchive != null) {
            File slPackg = new File(sharedLibraryArchive);
            //TODO: Check if exists
            InstallSharedLibraryRequest request =
                    new InstallSharedLibraryRequest(slPackg.toURI().toURL());

            String sharedLibraryName = getClient().installSharedLibrary(request);

            printMessage(getLocalizedString("cli.install-jbi-shared-library.success", sharedLibraryName));
        }
    }
}
