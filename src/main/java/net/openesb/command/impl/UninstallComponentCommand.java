package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.UninstallComponentRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.UNINSTALL_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.uninstall-jbi-component.descr")
public class UninstallComponentCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.uninstall-jbi-component.param.componentname",
            arity = 1)
    private List<String> componentNames;
    
    @Parameter(names = {"--force", "-F"},
            descriptionKey =  "cli.uninstall-jbi-component.param.force")
    private boolean force = false;
    
    @Override
    protected void executeRequest() throws Exception {
        String componentName = componentNames.iterator().next();
        
        if (componentName != null) {
            UninstallComponentRequest request = 
                    new UninstallComponentRequest(componentName);
            request.setForce(force);
            
            getClient().uninstallComponent(request);

            printMessage(getLocalizedString ("cli.uninstall-jbi-component.success", componentName));
        }
    }
}
