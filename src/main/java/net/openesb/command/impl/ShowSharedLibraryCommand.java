package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.command.utils.OutputUtils;
import net.openesb.model.api.SharedLibrary;
import net.openesb.sdk.model.GetSharedLibraryDescriptorRequest;
import net.openesb.sdk.model.GetSharedLibraryRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        separators = "=",
        commandNames = CommandConstants.SHOW_JBI_SHARED_LIBRARY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.show-jbi-shared-library.descr")
public class ShowSharedLibraryCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.show-jbi-shared-library.param.sharedlibraryname",
            required = true,
            arity = 1)
    private List<String> sharedLibraryNames;
    
    @Parameter(
            names = "--descriptor",
            descriptionKey = "cli.show-jbi-shared-library.param.descriptor")
    private boolean descriptor = false;

    @Override
    protected void executeRequest() throws Exception {
        String sharedLibraryName = sharedLibraryNames.iterator().next();

        if (sharedLibraryName != null) {
            GetSharedLibraryRequest request = new GetSharedLibraryRequest(sharedLibraryName);
            SharedLibrary sharedLibrary = getClient().getSharedLibrary(request);

            String sharedLibraryVersion = sharedLibrary.getVersion();
            String sharedLibraryBuildNumber = sharedLibrary.getBuildNumber();
            String sharedLibraryDescription = OutputUtils.multiLineFormat(
                    sharedLibrary.getDescription(), 70, 14).trim();

            String header = getLocalizedString("cli.sharedlibrary.header");
            printMessage(header);
            printMessage(OutputUtils.createFillString('-', header.length()));

            printMessage(getLocalizedString("cli.sharedlibrary.name", new Object[]{sharedLibraryName}));
            printMessage(getLocalizedString("cli.sharedlibrary.version", new Object[]{sharedLibraryVersion}));
            printMessage(getLocalizedString("cli.sharedlibrary.buildnumber", new Object[]{sharedLibraryBuildNumber}));
            printMessage(getLocalizedString("cli.sharedlibrary.description", new Object[]{sharedLibraryDescription}));
            
            if (descriptor) {
                displayOptionHeader("cli.component.descriptor.header",0);
                GetSharedLibraryDescriptorRequest requestDescr = new GetSharedLibraryDescriptorRequest(sharedLibraryName);
                String result = getClient().getSharedLibraryDescriptor(requestDescr);
                displayDescriptor(result);
            }
        }
    }
}
