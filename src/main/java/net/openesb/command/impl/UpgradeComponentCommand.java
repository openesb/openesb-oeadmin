package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.File;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.UninstallComponentRequest;
import net.openesb.sdk.model.UpgradeComponentRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.UPGRADE_JBI_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.upgrade-jbi-component.descr")
public class UpgradeComponentCommand extends AbstractCommand {

    @Parameter(names = {"--upgradefile"}, 
            descriptionKey = "cli.upgrade-jbi-component.param.upgradefile",
            arity = 1)
    private String upgradeFilePath;
    
    @Parameter(
            descriptionKey = "cli.upgrade-jbi-component.param.componentname",
            arity = 1)
    private List<String> componentNames;
    
    @Override
    protected void executeRequest() throws Exception {
        String componentName = componentNames.iterator().next();
        
        if (componentName != null) {
            File file = new File(upgradeFilePath);
            //TODO: checks if exists
            UpgradeComponentRequest request = 
                    new UpgradeComponentRequest(componentName, file.toURI().toURL());

            getClient().upgradeComponent(request);

            printMessage(getLocalizedString ("cli.upgrade-jbi-component.success", componentName));
        }
    }
}
