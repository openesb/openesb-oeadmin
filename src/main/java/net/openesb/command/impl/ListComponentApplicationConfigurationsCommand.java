package net.openesb.command.impl;

import com.beust.jcommander.Parameters;
import net.openesb.command.CommandConstants;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.LIST_APPLICATION_CONFIGURATIONS,
        commandDescription = "Lists the application configurations for the specified component.")
public class ListComponentApplicationConfigurationsCommand extends AbstractCommand {
    
    @Override
    protected void executeRequest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
