package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.ShutdownServiceAssemblyRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.SHUT_DOWN_SERVICE_ASSEMBLY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.shut-down-jbi-service-assembly.descr")
public class ShutdownServiceAssemblyCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.shut-down-jbi-service-assembly.param.serviceassemblyname",
            required = true,
            arity = 1)
    private List<String> serviceAssemblyNames;
    
    @Parameter(names = {"--force", "-F"},
            descriptionKey =  "cli.shut-down-jbi-service-assembly.param.force")
    private boolean force = false;
    
    @Override
    protected void executeRequest() throws Exception {
        String serviceAssemblyName = serviceAssemblyNames.iterator().next();

        if (serviceAssemblyName != null) {
            ShutdownServiceAssemblyRequest request = 
                    new ShutdownServiceAssemblyRequest(serviceAssemblyName);
            request.setForce(force);
            
            getClient().shutdownServiceAssembly(request);

            printMessage(getLocalizedString ("cli.shut-down-jbi-service-assembly.success", serviceAssemblyName));
        }
    }
}
