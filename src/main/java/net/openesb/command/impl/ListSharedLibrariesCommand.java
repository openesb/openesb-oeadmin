package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.Set;
import net.openesb.command.CommandConstants;
import net.openesb.model.api.SharedLibrary;
import net.openesb.sdk.model.ListSharedLibrariesRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.LIST_SHARED_LIBRARIES,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.list-jbi-shared-libraries.descr")
public class ListSharedLibrariesCommand extends AbstractCommand {

    @Parameter(names = {"--componentname"},
            descriptionKey =  "cli.list-jbi-shared-libraries.param.componentname",
            arity = 1)
    private String componentName;

    @Override
    protected void executeRequest() throws Exception {
        ListSharedLibrariesRequest request = new ListSharedLibrariesRequest();
        request.setComponentName(componentName);

        Set<SharedLibrary> sharedLibraries = getClient().listSharedLibraries(request);
        if (sharedLibraries.isEmpty()) {
            printMessage(getLocalizedString ("cli.list-jbi-shared-libraries.empty"));
        } else {
            for (SharedLibrary sharedLibrary : sharedLibraries) {
                printMessage(sharedLibrary.getName());
            }
        }
    }
}
