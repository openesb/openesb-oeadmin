package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.File;
import java.net.URL;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.InstallComponentRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.INSTALL_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.install-jbi-component.descr")
public class InstallComponentCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.install-jbi-component.param.path",
            arity = 1)
    private List<String> componentArchives;

    @Override
    protected void executeRequest() throws Exception {
        String componentArchive = componentArchives.iterator().next();

        if (componentArchive != null) {
            File cmpPackg = new File(componentArchive);
            //TODO: Check if exists
            InstallComponentRequest request =
                    new InstallComponentRequest(cmpPackg.toURI().toURL());

            String componentName = getClient().installComponent(request);

            printMessage(getLocalizedString("cli.install-jbi-component.success", componentName));
        }
    }
}
