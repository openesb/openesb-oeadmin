package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.Set;
import net.openesb.command.CommandConstants;
import net.openesb.command.internal.jcommander.StateConverter;
import net.openesb.model.api.JBIComponent;
import net.openesb.model.api.State;
import net.openesb.sdk.model.ListComponentsRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.LIST_COMPONENTS,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.list-jbi-components.descr")
public class ListComponentsCommand extends AbstractCommand {

    @Parameter(names = {"--libraryname"}, 
            descriptionKey = "cli.list-jbi-components.param.libraryname",
            arity = 1)
    private String libraryName;
    
    @Parameter(names = {"--lifecyclestate"},
            descriptionKey = "cli.list-jbi-components.param.lifecyclestate",
            converter = StateConverter.class,
            arity = 1)
    private State lifecycleState;
    
    @Parameter(names = {"--assemblyname"},
            descriptionKey = "cli.list-jbi-components.param.assemblyname",
            arity = 1)
    private String assemblyName;
    
    @Override
    protected void executeRequest() {
        ListComponentsRequest request = new ListComponentsRequest();
        request.setServiceAssemblyName(assemblyName);
        request.setSharedLibraryName(libraryName);
        request.setState(lifecycleState);
        
        Set<JBIComponent> components = getClient().listComponents(request);
        if (components.isEmpty()) {
            printMessage(getLocalizedString ("cli.list-jbi-components.empty"));
        } else {
            for (JBIComponent component : components) {
                printMessage(component.getName());
            }
        }
    }
}
