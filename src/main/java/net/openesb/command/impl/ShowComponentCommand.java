package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import java.util.Set;
import net.openesb.command.CommandConstants;
import net.openesb.model.api.ComponentConfiguration;
import net.openesb.model.api.JBIComponent;
import net.openesb.model.api.State;
import net.openesb.sdk.model.ListComponentConfigurationsRequest;
import net.openesb.sdk.model.GetComponentDescriptorRequest;
import net.openesb.sdk.model.GetComponentRequest;
import static net.openesb.command.utils.OutputUtils.*;
import net.openesb.model.api.Logger;
import net.openesb.sdk.model.ListComponentLoggersRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        separators = "=",
        commandNames = CommandConstants.SHOW_JBI_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.show-jbi-component.descr")
public class ShowComponentCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.show-jbi-component.param.componentname",
            required = true,
            arity = 1)
    private List<String> componentNames;
    @Parameter(
            names = "--descriptor",
            descriptionKey = "cli.show-jbi-component.param.descriptor")
    private boolean descriptor = false;
    @Parameter(
            names = "--loggers",
            descriptionKey = "cli.show-jbi-component.param.loggers")
    private boolean loggers = false;
    @Parameter(
            names = "--configuration",
            descriptionKey = "cli.show-jbi-component.param.configuration")
    private boolean configuration = false;

    @Override
    protected void executeRequest() throws Exception {
        String componentName = componentNames.iterator().next();

        if (componentName != null) {
            GetComponentRequest request = new GetComponentRequest(componentName);
            JBIComponent component = getClient().getComponent(request);

            String componentState = component.getState().toString();
            String componentVersion = component.getVersion();
            String componentBuildNumber = component.getBuildNumber();
            String componentDescription = multiLineFormat(
                    component.getDescription(), 70, 14).trim();

            String header = getLocalizedString("cli.component.header");
            printMessage(header);
            printMessage(createFillString('-', header.length()));

            printMessage(getLocalizedString("cli.component.name", new Object[]{componentName}));
            printMessage(getLocalizedString("cli.component.state", new Object[]{componentState}));
            printMessage(getLocalizedString("cli.component.version", new Object[]{componentVersion}));
            printMessage(getLocalizedString("cli.component.buildnumber", new Object[]{componentBuildNumber}));
            printMessage(getLocalizedString("cli.component.description", new Object[]{componentDescription}));

            if (loggers) {
                displayOptionHeader("cli.component.loggers.header",0);
                
                ListComponentLoggersRequest requestLoggers = 
                        new ListComponentLoggersRequest(componentName);
                Set<Logger> loggers = getClient().listComponentLoggers(requestLoggers);
            
                displayNameValuePairs(loggers, 0);
            }

            if (configuration) {
                displayOptionHeader("cli.component.configuration.header", 0);
                ListComponentConfigurationsRequest requestConf =
                        new ListComponentConfigurationsRequest(componentName);

                Set<ComponentConfiguration> configurations =
                        getClient().listComponentConfigurations(requestConf);

                if (configurations.isEmpty()) {
                    if (component.getState().equals(State.SHUTDOWN)) {
                        printMessage(
                                getLocalizedString("cli.show-jbi-component.warning"));
                    }
                } else {
                    displayNameValuePairs(configurations, 0);
                }
            }

            if (descriptor) {
                displayOptionHeader("cli.component.descriptor.header", 0);
                GetComponentDescriptorRequest requestDescr =
                        new GetComponentDescriptorRequest(componentName);

                String result = getClient().getComponentDescriptor(requestDescr);
                displayDescriptor(result);
            }
        }
    }
}
