package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.openesb.command.CommandConstants;
import net.openesb.command.utils.ApplicationVariableConverter;
import net.openesb.model.api.ApplicationVariable;
import net.openesb.sdk.model.UpdateComponentApplicationVariablesRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        separators = "=",
        commandNames = CommandConstants.UPDATE_APPLICATION_VARIABLE,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.update-jbi-application-variable.descr")
public class UpdateComponentApplicationVariableCommand extends AbstractCommand {

        @Parameter(
            names = "--component",
            descriptionKey = "cli.update-jbi-application-variable.param.component",
            required = true,
            arity = 1)
    private String componentName;
    @Parameter(
            descriptionKey = "cli.update-jbi-application-variable.param.value",
            required = true)
    private List<String> variables;
    
    @Override
    protected void executeRequest() throws Exception {
        String values = variables.get(0);
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(values));
        } catch (IOException e) {
            properties = createPropertiesParam(values);
        }

        UpdateComponentApplicationVariablesRequest request = new UpdateComponentApplicationVariablesRequest(componentName);

        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            ApplicationVariable appVariable = ApplicationVariableConverter.convert(
		    (String)entry.getKey(), (String)entry.getValue());

            if (appVariable != null) {
                request.addApplicationVariable(appVariable);
            }
        }

        getClient().updateComponentApplicationVariables(request);
    }
}
