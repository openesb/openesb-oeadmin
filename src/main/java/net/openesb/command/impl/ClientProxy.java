package net.openesb.command.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import net.openesb.sdk.OpenESBClient;
import net.openesb.sdk.model.AbstractRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ClientProxy implements java.lang.reflect.InvocationHandler {

    private final OpenESBClient obj;
    private final String username, password;
    
    public static OpenESBClient newInstance(OpenESBClient obj, String username, String password) {
        return (OpenESBClient) java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj
                .getClass().getInterfaces(), new ClientProxy(obj, username, password));
    }
    
    private ClientProxy(OpenESBClient obj, String username, String password) {
        this.obj = obj;
        this.username = username;
        this.password = password;
    }

    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        
        try {
            if (args[0] instanceof AbstractRequest) {
                ((AbstractRequest) args[0]).setUsername(username);
                ((AbstractRequest) args[0]).setPassword(password);
            }
            
            result = method.invoke(obj, args);
        } catch (InvocationTargetException e) {
            throw e.getTargetException(); 
        } catch (Exception e) {
            throw new RuntimeException("unexpected invocation exception: " + e.getMessage());
        }
        
        return result;
    }

    
}
