package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.Set;
import net.openesb.command.CommandConstants;
import net.openesb.model.api.ApplicationVariable;
import net.openesb.sdk.model.ListComponentApplicationVariablesRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.LIST_APPLICATION_VARIABLES,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.list-jbi-application-variables.descr")
public class ListComponentApplicationVariablesCommand extends AbstractCommand {
    
    @Parameter(
            names = "--component",
            descriptionKey = "cli.list-jbi-application-variables.param.component",
            required = true,
            arity = 1)
    private String componentName;
    
    @Override
    protected void executeRequest() throws Exception {
        ListComponentApplicationVariablesRequest request = 
                new ListComponentApplicationVariablesRequest(componentName);
        
        Set<ApplicationVariable> variables = 
                getClient().listComponentApplicationVariables(request);
        
        if (variables.isEmpty()) {
            printMessage(getLocalizedString ("cli.list-jbi-application-variables.empty"));
        } else {
            displayNameValuePairs(variables, 0);
        }
    }
}
