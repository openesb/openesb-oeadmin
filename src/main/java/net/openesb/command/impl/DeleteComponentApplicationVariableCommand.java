package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.DeleteComponentApplicationVariablesRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        separators = ",",
        commandNames = CommandConstants.DELETE_APPLICATION_VARIABLE,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.delete-jbi-application-variable.descr")
public class DeleteComponentApplicationVariableCommand extends AbstractCommand {

    @Parameter(
            names = "--component",
            descriptionKey = "cli.delete-jbi-application-variable.param.component",
            required = true,
            arity = 1)
    private String componentName;
    
    @Parameter(
            descriptionKey = "cli.delete-jbi-application-variable.param.value",
            required = true)
    private List<String> variables;
    
    @Override
    protected void executeRequest() throws Exception {
        for (String variable : variables) {
            DeleteComponentApplicationVariablesRequest request = new DeleteComponentApplicationVariablesRequest(componentName);
            request.addApplicationVariable(variable);

            getClient().deleteComponentApplicationVariables(request);
        }
    }
}
