package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.StopServiceAssemblyRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.STOP_SERVICE_ASSEMBLY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.stop-jbi-service-assembly.descr")
public class StopServiceAssemblyCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.stop-jbi-service-assembly.param.serviceassemblyname",
            required = true,
            arity = 1)
    private List<String> serviceAssemblyNames;
    
    @Override
    protected void executeRequest() throws Exception {
        String serviceAssemblyName = serviceAssemblyNames.iterator().next();

        if (serviceAssemblyName != null) {
            StopServiceAssemblyRequest request = 
                    new StopServiceAssemblyRequest(serviceAssemblyName);

            getClient().stopServiceAssembly(request);

            printMessage(getLocalizedString ("cli.stop-jbi-service-assembly.success", serviceAssemblyName));
        }
    }
}
