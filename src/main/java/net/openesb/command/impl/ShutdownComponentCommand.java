package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.ShutdownComponentRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.SHUT_DOWN_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.shut-down-jbi-component.descr")
public class ShutdownComponentCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.shut-down-jbi-component.param.componentname",
            required = true,
            arity = 1)
    private List<String> componentNames;
    
    @Parameter(names = {"--force", "-F"},
            descriptionKey =  "cli.shut-down-jbi-component.param.force")
    private boolean force = false;

    @Override
    protected void executeRequest() throws Exception {
        String componentName = componentNames.iterator().next();

        if (componentName != null) {
            ShutdownComponentRequest request =
                    new ShutdownComponentRequest(componentName);
            request.setForce(force);

            getClient().shutdownComponent(request);

            printMessage(getLocalizedString("cli.shut-down-jbi-component.success", componentName));
        }
    }
}
