package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.StartServiceAssemblyRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.START_SERVICE_ASSEMBLY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.start-jbi-service-assembly.descr")
public class StartServiceAssemblyCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.start-jbi-service-assembly.param.serviceassemblyname",
            required = true,
            arity = 1)
    private List<String> serviceAssemblyNames;
    
    @Override
    protected void executeRequest() throws Exception {
        String serviceAssemblyName = serviceAssemblyNames.iterator().next();

        if (serviceAssemblyName != null) {
            StartServiceAssemblyRequest request = 
                    new StartServiceAssemblyRequest(serviceAssemblyName);

            getClient().startServiceAssembly(request);

            printMessage(getLocalizedString ("cli.start-jbi-service-assembly.success", serviceAssemblyName));
        }
    }
}
