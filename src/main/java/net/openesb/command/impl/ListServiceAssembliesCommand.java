package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.Set;
import net.openesb.command.CommandConstants;
import net.openesb.command.internal.jcommander.StateConverter;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.State;
import net.openesb.sdk.model.ListServiceAssembliesRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.LIST_SERVICE_ASSEMBLIES,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.list-jbi-service-assemblies.descr")
public class ListServiceAssembliesCommand extends AbstractCommand {

    @Parameter(names = {"--componentname"},
            descriptionKey =  "cli.list-jbi-service-assemblies.param.componentname",
            arity = 1)
    private String componentName;
    @Parameter(names = {"--lifecyclestate"},
            descriptionKey =  "cli.list-jbi-service-assemblies.param.lifecyclestate",
            converter = StateConverter.class,
            arity = 1)
    private State lifecycleState;

    @Override
    protected void executeRequest() throws Exception {
        ListServiceAssembliesRequest request = new ListServiceAssembliesRequest();
        request.setComponentName(componentName);
        request.setState(lifecycleState);

        Set<ServiceAssembly> assemblies = getClient().listServiceAssemblies(request);
        if (assemblies.isEmpty()) {
            printMessage(getLocalizedString ("cli.list-jbi-service-assemblies.empty"));
        } else {
            for (ServiceAssembly assembly : assemblies) {
                printMessage(assembly.getName());
            }
        }
    }
}
