package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Set;
import net.openesb.command.Command;
import net.openesb.command.CommandException;
import net.openesb.command.utils.CLITokenizer;
import net.openesb.command.utils.I18NBundle;
import net.openesb.sdk.OpenESBClient;
import net.openesb.sdk.http.OpenESBClientImpl;
import static net.openesb.command.utils.OutputUtils.*;
import net.openesb.model.api.NameValuePair;
import net.openesb.sdk.OpenESBClientException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class AbstractCommand implements Command {

    @Parameter(
	    names = {"--user", "-u"},
	    descriptionKey = "cli.inherited.param.user",
	    arity = 1)
    private String userName;
    @Parameter(
	    names = {"--passwordfile"},
	    descriptionKey = "cli.inherited.param.passwordfile",
	    converter = FileConverter.class,
	    arity = 1)
    private File passwordFile;
    @Parameter(names = {"--host", "-H"},
	    descriptionKey = "cli.inherited.param.host",
	    arity = 1)
    private String host = "localhost";
    @Parameter(names = {"--port", "-p"},
	    descriptionKey = "cli.inherited.param.port",
	    arity = 1)
    private int port = 4848;
    @Parameter(
	    names = "--help",
	    help = true)
    private boolean help;
    private static OpenESBClient client;

    protected synchronized OpenESBClient getClient() {
	if (client == null) {
	    client = initClient();
	}
	return client;
    }

    private OpenESBClient initClient() {
	String url = String.format("http://%s:%s/openesb/api", this.host, this.port);
        return ClientProxy.newInstance(
                new OpenESBClientImpl(url), getUserName(), readPassword());
    }

    protected String getLocalizedString(String key, Object[] args) {
	return I18NBundle.getBundle().getMessage(key, args);
    }

    protected String getLocalizedString(String key, Object arg) {
	return I18NBundle.getBundle().getMessage(key, arg);
    }

    protected String getLocalizedString(String key) {
	return getLocalizedString(key, null);
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public File getPasswordFile() {
	return passwordFile;
    }

    public void setPasswordFile(File passwordFile) {
	this.passwordFile = passwordFile;
    }

    public String getHost() {
	return host;
    }

    public void setHost(String host) {
	this.host = host;
    }

    public int getPort() {
	return port;
    }

    public void setPort(int port) {
	this.port = port;
    }

    protected void printMessage(String message) {
	System.out.println(message);
    }

    /**
     * Will extract the header using the given bundleKey and display it to the
     * screen.
     *
     * @param bundleKey The property key value
     * @param indention The amount to indent the string
     */
    protected void displayOptionHeader(String bundleKey, int indention) {
	String msg = getLocalizedString(bundleKey);
	displayHeader(msg, indention);
    }

    protected void displayHeader(String headerString, int indention) {
	displayMessage(" ", 0);
	displayMessage(headerString, indention);
	String underline = createFillString('-', headerString.length());
	displayMessage(underline, indention);
    }

    /**
     * Will display the descriptor result (xml file)
     *
     * @param descriptor string (xml format)
     */
    protected void displayDescriptor(String descriptor) {
	displayMessage(descriptor, 0);
    }

    /**
     * Formulate and Returns Properties from the given string
     *
     * @return Properties
     */
    protected Properties createPropertiesParam(final String propertyStr) throws CommandException {
	if (propertyStr == null) {
	    return null;
	}
	Properties properties = new Properties();
	final CLITokenizer propertyTok = new CLITokenizer(propertyStr, ",");
	while (propertyTok.hasMoreTokens()) {
	    final String nameAndvalue = propertyTok.nextToken();
	    final CLITokenizer nameTok = new CLITokenizer(nameAndvalue, "=");
	    if (nameTok.countTokens() == 2) {
		properties.setProperty(nameTok.nextTokenWithoutEscapeAndQuoteChars(),
			nameTok.nextTokenWithoutEscapeAndQuoteChars());
	    } else {
		throw new CommandException("InvalidPropertySyntax");
	    }
	}
	return properties;
    }

    /**
     * Will display the content of a Set sorted by the keys. The display format
     * will be key = value, one entry per line.
     *
     * @param map The map to display
     */
    protected void displayNameValuePairs(Set<? extends NameValuePair<?>> pairs, int indentAmount) {
	for (NameValuePair<?> pair : pairs) {
	    String outputString = createFillString(' ', indentAmount)
		    + pair.getName() + " = " + pair.getValue();
	    printMessage(outputString);
	}
    }

    @Override
    public boolean execute() {
	try {
	//    getClient().login(new LoginRequest(
	//	    getUserName(), readPassword()));

	    executeRequest();

	//    getClient().logout(new LogoutRequest());
	} catch (OpenESBClientException oex) {
	    printMessage(oex.getMessage());
	    return false;
	} catch (Exception e) {
	    printMessage(e.getMessage());
	    return false;
	}

	return true;
    }

    @Override
    public boolean help() {
	return help;
    }

    private String readPassword() {
	// First, try to read password from password file
	String password = readPasswordFromFile();

	if (password == null) {
            // Password can't be read from password file
	    // Let's try by asking user.
	    password = new String(readPasswordFromConsole());
	}

	return password;
    }

    private String readPasswordFromFile() {
	if (passwordFile != null && passwordFile.exists()) {
	    Properties properties = new Properties();
	    try {
		properties.load(new FileInputStream(passwordFile));
		return properties.getProperty("OE_ADMIN_PASSWORD");
	    } catch (IOException e) {
	    }
	}

	return null;
    }

    public char[] readPasswordFromConsole() {
	try {
	    System.out.print("Password: ");
	    InputStreamReader isr = new InputStreamReader(System.in);
	    BufferedReader in = new BufferedReader(isr);
	    String result = in.readLine();
	    in.close();
	    isr.close();
	    return result.toCharArray();
	} catch (IOException e) {
	    throw new ParameterException(e);
	}
    }

    protected abstract void executeRequest() throws Exception;
}
