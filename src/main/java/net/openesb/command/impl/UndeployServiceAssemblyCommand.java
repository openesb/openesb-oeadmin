package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.UndeployServiceAssemblyRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.UNDEPLOY_SERVICE_ASSEMBLY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.undeploy-jbi-service-assembly.descr")
public class UndeployServiceAssemblyCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.undeploy-jbi-service-assembly.param.serviceassemblyname",
            required = true,
            arity = 1)
    private List<String> serviceAssemblyNames;
    
    @Parameter(names = {"--force", "-F"},
            descriptionKey =  "cli.undeploy-jbi-service-assembly.param.force")
    private boolean force = false;
    
    @Override
    protected void executeRequest() throws Exception {
        String serviceAssemblyName = serviceAssemblyNames.iterator().next();

        if (serviceAssemblyName != null) {
            UndeployServiceAssemblyRequest request = 
                    new UndeployServiceAssemblyRequest(serviceAssemblyName);
            request.setForce(force);
            
            getClient().undeployServiceAssembly(request);

            printMessage(getLocalizedString ("cli.undeploy-jbi-service-assembly.success", serviceAssemblyName));
        }
    }
}
