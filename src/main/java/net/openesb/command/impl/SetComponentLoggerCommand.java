package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.openesb.command.CommandConstants;
import net.openesb.model.api.Logger;
import net.openesb.sdk.model.SetComponentLoggerRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        separators = "=",
        commandNames = CommandConstants.SET_JBI_COMPONENT_LOGGER,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.set-jbi-component-logger.descr")
public class SetComponentLoggerCommand extends AbstractCommand {

    @Parameter(
            names = "--component",
            descriptionKey = "cli.set-jbi-component-logger.param.component",
            required = true,
            arity = 1)
    private String componentName;
    @Parameter(
            descriptionKey = "cli.set-jbi-component-logger.param.value",
            required = true)
    private List<String> loggers;
    
    @Override
    protected void executeRequest() throws Exception {
        String values = loggers.get(0);
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(values));
        } catch (IOException e) {
            properties = createPropertiesParam(values);
        }

        for (Map.Entry<Object, Object> config : properties.entrySet()) {
            String loggerName = ((String) config.getKey()).trim();
            String loggerLevel = ((String) config.getValue()).trim();
            
            SetComponentLoggerRequest request =
                new SetComponentLoggerRequest(
                    componentName, 
                    new Logger(loggerName, loggerLevel));
            
            getClient().setComponentLogger(request);
        }
    }
}
