package net.openesb.command.impl;

import com.beust.jcommander.Parameters;
import net.openesb.command.CommandConstants;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.CREATE_APPLICATION_CONFIGURATION,
        commandDescription = "Creates an application configuration for the specified component.")
public class CreateComponentApplicationConfigurationCommand extends AbstractCommand {

    @Override
    protected void executeRequest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
