package net.openesb.command.impl;

import com.beust.jcommander.Parameters;
import net.openesb.command.CommandConstants;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.DELETE_APPLICATION_CONFIGURATION,
        commandDescription = "Deletes the application configuration for the given component.")
public class DeleteComponentApplicationConfigurationCommand extends AbstractCommand {

    @Override
    protected void executeRequest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
