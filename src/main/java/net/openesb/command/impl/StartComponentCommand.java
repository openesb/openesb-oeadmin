package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.StartComponentRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.START_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.start-jbi-component.descr")
public class StartComponentCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.start-jbi-component.param.componentname",
            required = true,
            arity = 1)
    private List<String> componentNames;
    
    @Override
    protected void executeRequest() throws Exception {
        String componentName = componentNames.iterator().next();

        if (componentName != null) {
            StartComponentRequest request = 
                    new StartComponentRequest(componentName);

            getClient().startComponent(request);

            printMessage(getLocalizedString ("cli.start-jbi-component.success", componentName));
        }
    }
}
