package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.openesb.command.CommandConstants;
import net.openesb.model.api.ComponentConfiguration;
import net.openesb.sdk.model.SetComponentConfigurationRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.SET_JBI_COMPONENT_CONFIGURATION,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.set-jbi-component-configuration.descr")
public class SetComponentConfigurationCommand extends AbstractCommand {

    @Parameter(
            names = "--component",
            descriptionKey = "cli.set-jbi-component-configuration.param.component",
            required = true,
            arity = 1)
    private String componentName;
    
    @Parameter(
            descriptionKey = "cli.set-jbi-component-configuration.param.value",
            required = true)
    private List<String> configurations;

    @Override
    protected void executeRequest() throws Exception {
        String values = configurations.get(0);
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(values));
        } catch (IOException e) {
            properties = createPropertiesParam(values);
        }

        SetComponentConfigurationRequest request =
                new SetComponentConfigurationRequest(componentName);

        for (Map.Entry<Object, Object> config : properties.entrySet()) {
            String value = (String) config.getValue();
            // By default, this is string value
            
            Object objValue = value;

            if (isBoolean(value)) {
                objValue = Boolean.valueOf(value);
            } else if (isInteger(value)) {
                objValue = Integer.parseInt(value);
            }

            request.addComponentConfiguration(
                    new ComponentConfiguration(
                    (String) config.getKey(), objValue));
        }

        getClient().setComponentConfigurations(request);
    }

    private boolean isBoolean(String value) {
        return value != null && (value.equalsIgnoreCase(Boolean.TRUE.toString())
                || value.equalsIgnoreCase(Boolean.FALSE.toString()));
    }

    private boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
