package net.openesb.command.impl;

import com.beust.jcommander.Parameters;
import net.openesb.command.CommandConstants;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.SET_JBI_RUNTIME_LOGGER,
        commandDescription = "Sets the log levels for a logger in the JBI runtime environment.")
public class SetRuntimeLoggerCommand extends AbstractCommand {

    @Override
    protected void executeRequest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
