package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.UninstallSharedLibraryRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.UNINSTALL_SHARED_LIBRARY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.uninstall-jbi-shared-library.descr")
public class UninstallSharedLibraryCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.uninstall-jbi-shared-library.param.sharedlibraryname",
            arity = 1)
    private List<String> sharedLibraryNames;

    @Override
    protected void executeRequest() throws Exception {
        String sharedLibraryName = sharedLibraryNames.iterator().next();
        
        if (sharedLibraryName != null) {
            UninstallSharedLibraryRequest request = 
                    new UninstallSharedLibraryRequest(sharedLibraryName);
            
            getClient().uninstallSharedLibrary(request);

            printMessage(getLocalizedString ("cli.uninstall-jbi-shared-library.success", sharedLibraryName));
        }
    }
}
