package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.command.utils.OutputUtils;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.ServiceUnit;
import net.openesb.sdk.model.GetServiceAssemblyRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.SHOW_JBI_SERVICE_ASSEMBLY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.show-jbi-service-assembly.descr")
public class ShowServiceAssemblyCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.show-jbi-service-assembly.param.serviceassemblyname",
            required = true,
            arity = 1)
    private List<String> serviceAssemblyNames;

    @Override
    protected void executeRequest() throws Exception {
        String serviceAssemblyName = serviceAssemblyNames.iterator().next();

        if (serviceAssemblyName != null) {
            GetServiceAssemblyRequest request = new GetServiceAssemblyRequest(serviceAssemblyName);
            ServiceAssembly assembly = getClient().getServiceAssembly(request);

            String assemblyName = assembly.getName();
            String assemblyDescription = OutputUtils.multiLineFormat(
                    assembly.getDescription(), 70, 14).trim();
            String assemblyState = assembly.getState().toString();

            String header = getLocalizedString("cli.serviceassembly.header");
            printMessage(header);
            printMessage(OutputUtils.createFillString('-', header.length()));

            printMessage(getLocalizedString("cli.serviceassembly.name", new Object[]{assemblyName}));
            printMessage(getLocalizedString("cli.serviceassembly.state", new Object[]{assemblyState}));
            printMessage(getLocalizedString("cli.serviceassembly.description",  new Object[]{assemblyDescription}));

            String indentString = "    ";
            printMessage("");
            String serviceUnitsHeader = getLocalizedString("cli.serviceunit.header");
            printMessage(indentString + serviceUnitsHeader);
            printMessage(indentString + OutputUtils.createFillString('-', serviceUnitsHeader.length()));
            boolean firstTime = true;
            
            for(ServiceUnit serviceUnit : assembly.getServiceUnits()) {
                String suState = serviceUnit.getState().toString();
                String suDepoyedOn = serviceUnit.getDeployedOn();
                String suName = serviceUnit.getName();
                String suDescription = OutputUtils.multiLineFormat(
                        serviceUnit.getDescription(), 50, 18);
                
                if (!(firstTime)) {
                    printMessage("");
                }
                printMessage(indentString
                        + getLocalizedString("cli.serviceunit.name", new Object[]{suName}));
                printMessage(indentString
                        + getLocalizedString("cli.serviceunit.state", new Object[]{suState}));
                printMessage(indentString
                        + getLocalizedString("cli.serviceunit.deployedon", new Object[]{suDepoyedOn}));
                printMessage(indentString
                        + getLocalizedString("cli.serviceunit.description", new Object[]{suDescription}));
                firstTime = false;
            }
        }
    }
}
