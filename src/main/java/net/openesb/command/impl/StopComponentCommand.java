package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.StopComponentRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.STOP_COMPONENT,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.stop-jbi-component.descr")
public class StopComponentCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.stop-jbi-component.param.componentname",
            required = true,
            arity = 1)
    private List<String> componentNames;
    
    @Override
    protected void executeRequest() throws Exception {
        String componentName = componentNames.iterator().next();

        if (componentName != null) {
            StopComponentRequest request = 
                    new StopComponentRequest(componentName);

            getClient().stopComponent(request);

            printMessage(getLocalizedString ("cli.stop-jbi-component.success", componentName));
        }
    }
}
