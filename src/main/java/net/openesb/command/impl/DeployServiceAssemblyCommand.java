package net.openesb.command.impl;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.File;
import java.util.List;
import net.openesb.command.CommandConstants;
import net.openesb.sdk.model.DeployServiceAssemblyRequest;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Parameters(
        commandNames = CommandConstants.DEPLOY_SERVICE_ASSEMBLY,
        resourceBundle = CommandConstants.BUNDLE_NAME,
        commandDescriptionKey = "cli.deploy-jbi-service-assembly.descr")
public class DeployServiceAssemblyCommand extends AbstractCommand {

    @Parameter(
            descriptionKey = "cli.deploy-jbi-service-assembly.param.path",
            arity = 1)
    private List<String> saArchives;

    @Override
    protected void executeRequest() throws Exception {
        String saArchive = saArchives.iterator().next();

        if (saArchive != null) {
            File saPackg = new File(saArchive);
            //TODO: Check if exists
            DeployServiceAssemblyRequest request =
                    new DeployServiceAssemblyRequest(saPackg.toURI().toURL());

            String serviceAssemblyName = getClient().deployServiceAssembly(request);

            printMessage(getLocalizedString("cli.deploy-jbi-service-assembly.success", serviceAssemblyName));
        }
    }
}
