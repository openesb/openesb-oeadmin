package net.openesb.command;

import net.openesb.command.internal.CommandLineParserFactory;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class CommandCli {
    
    public static void main(String[] args) {
        CommandLineParserFactory.getInstance().execute(args);
    }
}
