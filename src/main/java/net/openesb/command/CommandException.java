package net.openesb.command;

/**
 *
 * @author david
 */
public class CommandException extends java.lang.Exception
{

    /**
     * Creates new <code>CommandException</code> without detail message.
     */
    public CommandException()
    {
    }


    /**
     * Constructs an <code>CommandException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public CommandException(String msg)
    {
        super(msg);
    }


    /**
     *  Constructs a new <code>CommandException</code> exception with the specified
     *  cause
     */
    public CommandException(Throwable cause)
    {
	super(cause);
    }


    /**
     *  Constructs a new <code>CommandException</code> exception with the specified
     *  detailed message and cause
     */
    public CommandException(String msg, Throwable cause)
    {
	super(msg, cause);
    }

}
