== delete-jbi-application-configuration

<<Name>> | <<Synopsis>> | <<Description>> | <<Options>> | <<Operands>> | <<Examples>>

[[Name]]
.Name
delete-jbi-application-configuration – deletes the application configuration for the given component

[[Synopsis]]
.Synopsis
[source,java]
----
	delete-jbi-application-configuration
	[--host host] [--port port] 
	[--user admin_user]
	[--passwordfile filename] [--help]
	--component component name name
----

[[Description]]
.Description
The _delete-jbi-application-configuration_ command deletes the application configuration for the given component.

[[Options]]
.Options
*TODO*

[[Operands]]
.Operands
name::
The name of the application configuration that will be deleted.

[[Examples]]
.Examples
*Example 1 Using the delete-jbi-application-configuration command*
[source,bash]
----
> oeadmin delete-jbi-application-configuration --component=cli-config-binding testConfig
Command delete-jbi-application-configuration executed successfully.
----