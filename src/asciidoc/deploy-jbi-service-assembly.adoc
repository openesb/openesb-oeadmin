== deploy-jbi-service-assembly

<<Name>> | <<Synopsis>> | <<Description>> | <<Options>> | <<Operands>> | <<Examples>>

[[Name]]
.Name
deploy-jbi-service-assembly – deploys a service assembly into the JBI environment

[[Synopsis]]
.Synopsis
[source,java]
----
	deploy-jbi-service-assembly 
	[--host host] [--port port] 
	[--user admin_user]
	[--passwordfile filename] [--help]
	[--enabled =true]
	[--upload=true]
	filepath
----

[[Description]]
.Description
The _deploy-jbi-service-assembly_ command deploys a service assembly into the JBI environment.

[[Options]]
.Options
*TODO*

[[Operands]]
.Operands
filepath::
The path to the archive file that contains the attributes of the JBI service assembly.

[[Examples]]
.Examples
*Example 1 Using the deploy-jbi-service-assembly command*
[source,bash]
----
> oeadmin deploy-jbi-service-assembly --user admin2 --passwordfile passwords.txt filepath
Command deploy-jbi-service-assembly executed successfully.
----
